"""
Nose tests for mongo_brevets.py
"""

from mongo_brevets import submit
import flask
from werkzeug.datastructures import ImmutableMultiDict
from nose.tools import *    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_submit():
    """

    """

    assert submit(ImmutableMultiDict([('km', '50'), ('km', '100'), ('open', 'Sun 1/1 1:28'), ('open', 'Sun 1/1 2:56'), ('close', 'Sun 1/1 3:30'), ('close', 'Sun 1/1 6:40')])) == [{'open': 'Sun 1/1 2:56', 'close': 'Sun 1/1 6:40', 'km': '100'}, {'open': 'Sun 1/1 1:28', 'close': 'Sun 1/1 3:30', 'km': '50'}]
