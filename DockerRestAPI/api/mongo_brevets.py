"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import (redirect, url_for, request, render_template, flash,
                    abort, Response, make_response, g, jsonify, session)
from flask_httpauth import HTTPTokenAuth, HTTPBasicAuth, MultiAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from passlib.hash import pbkdf2_sha256 as Pass
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
from flask_restful import Resource, Api, reqparse
import logging
import csv
import io
from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, PasswordField
from wtforms.validators import DataRequired
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
token_serializer = Serializer(app.config['SECRET_KEY'], expires_in=3600)
token_auth = HTTPTokenAuth('Bearer')
basic_auth = HTTPBasicAuth()
multi_auth = MultiAuth(basic_auth, token_auth)
parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('password')
parser.add_argument('remember')
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Step 1: create a client object
# The environment variable DB_PORT_27017_TCP_ADDR is the IP of a linked docker
# container (IP of the database). Generally "DB_PORT_27017_TCP_ADDR" can be set to
# "localhost" or "127.0.0.1".
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB
db = client.tododb
securedb = client.securedb

class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me')
    submit = SubmitField('Submit')

class User(UserMixin):
    def __init__(self, id, active=True):
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

def submit(f):
    opens = []
    closes = []
    kms = []
    items = []
    for key in f.keys():
        for value in f.getlist(key):
            if (key == "open" and value != ""):
                opens.append(value)
            elif (key == "close" and value != ""):
                closes.append(value)
            elif (key == "km" and value != ""):
                kms.append(value)

    while (len(opens) > 0):
        items.append({
            'open': opens.pop(),
            'close': closes.pop(),
            'km': kms.pop()
        })
    return items


###
# Pages
###

@app.route("/")
@app.route("/index")
@login_required
def index():
    return flask.render_template('index.html')

@app.route("/calc")
@login_required
def calc():
    return flask.render_template('calc.html')

@app.route("/register", methods=['GET', 'POST'])
def registerPage():
    form = RegisterForm()
    if form.validate_on_submit():
        args = parser.parse_args()
        hashedPass = Pass.hash(args['password'])
        db.securedb.insert_one({'username': args['username'], 'password': hashedPass})
        return flask.redirect(flask.url_for('index'))
    return flask.render_template('register.html', form=form)

@app.route("/users")
def users():
    abort(401)

@app.errorhandler(404)
def page_not_found(error):
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    args = parser.parse_args()
    if form.validate_on_submit():
        _user = db.securedb.find({'username': args['username']})
        user = [item for item in _user]
        if (len(user) == 0):
            flash('Username not found!', 'noUser')
            return flask.render_template('login.html', form=form)
        if (Pass.verify(args['password'], user[0]['password'])):
            if(args['remember'] == None):
                login_user(User(user[0]['_id']), remember=False)
            login_user(User(user[0]['_id']), remember=True)
            token = token_serializer.dumps({'username': args['username']}).decode('utf-8')
            session['token'] = token
            next = flask.request.args.get('next')
            return flask.redirect(next or flask.url_for('index'))
    return flask.render_template('login.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    session['token'] = None
    return redirect(url_for("index"))

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_km = request.args.get('brevet_km', type=float)
    start_time = arrow.get(request.args.get('start_time', type=str))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # Updated these to use values from the page
    open_time = acp_times.open_time(km, brevet_km, start_time.isoformat())
    close_time = acp_times.close_time(km, brevet_km, start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/_submit', methods=['POST'])
def new():
    f = request.form
    items = submit(f)
    while (len(items) > 0):
        db.tododb.insert_one(items.pop())
    return ('', 204)

@app.route('/_display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if (len(items) == 0):
        return flask.render_template('empty.html')
    else:
        return render_template('todo.html', items=items)
#############
# RESTful api
#############
class ListAll(Resource):
    @token_auth.login_required
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
        return items

class ListAllType(Resource):
    @token_auth.login_required
    def get(self, type):
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["open","close"])
            for times in items:
                writer.writerow([times['open'],times['close']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return flask.render_template('404.html'), 404

class ListOpen(Resource):
    @token_auth.login_required
    def get(self):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['close']
            id['sort'] = arrow.get(id['open'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            # Shoutout to Mario F on stack exchange for this line on sort a list of dicts
            # https://stackoverflow.com/questions/72899/how-do-i-sort-a-list-of-dictionaries-by-a-value-of-the-dictionary
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        return items

class ListOpenType(Resource):
    @token_auth.login_required
    def get(self, type):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['close']
            id['sort'] = arrow.get(id['open'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["open"])
            for times in items:
                writer.writerow([times['open']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return items

class ListClose(Resource):
    @token_auth.login_required
    def get(self):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['open']
            id['sort'] = arrow.get(id['close'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        return items

class ListCloseType(Resource):
    @token_auth.login_required
    def get(self, type):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['open']
            id['sort'] = arrow.get(id['close'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["close"])
            for times in items:
                writer.writerow([times['close']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return items

class AddTimes(Resource):
    @token_auth.login_required
    def post(self):
        items = [{'open': 'Sun 1/1 1:28', 'close': 'Sun 1/1 3:30', 'km': '50'}, {'open': 'Sun 1/1 1:11', 'close': 'Sun 1/1 3:00', 'km': '50'}, {'open': 'Sun 1/1 0:53', 'close': 'Sun 1/1 2:30', 'km': '30'}, {'open': 'Sun 1/1 0:35', 'close': 'Sun 1/1 2:00', 'km': '20'}, {'open': 'Sun 1/1 0:18', 'close': 'Sun 1/1 1:30', 'km': '10'}]
        while (len(items) > 0):
            db.tododb.insert_one(items.pop())
        return ('', 204)

class Register(Resource):
    def post(self):
        args = parser.parse_args()
        hashedPass = Pass.hash(args['password'])
        db.securedb.insert_one({'username': args['username'], 'password': hashedPass})
        resp = jsonify({'username': args['username']})
        resp.statust_code = 201
        resp.headers['location'] = '/users/{}'.format(db.securedb.count())
        return resp

class Token(Resource):
    @basic_auth.login_required
    def get(self):
        token = token_serializer.dumps({'username': g.user}).decode('utf-8')
        session['token'] = token
        return {'duration': 3600, 'token': token }

@token_auth.verify_token
def verify_token(token):
    try:
        data = token_serializer.loads(session['token'])
        return True
    except:
        pass # noqa: E722
    try:
        data = token_serializer.loads(token)
        return True
    except:
        return False
    return False

@basic_auth.verify_password
def verify_password(username, password):
    _user = db.securedb.find({'username': username}, {'password': 1, '_id': 0})
    user = [item for item in _user]
    if (len(user) == 0):
        abort(401)
    if (Pass.verify(password, user[0]['password'])):
        g.user = username
        return True
    return False



api.add_resource(ListAll, '/listAll/')
api.add_resource(ListAllType, '/listAll/<type>')
api.add_resource(ListOpen, '/listOpenOnly')
api.add_resource(ListOpenType, '/listOpenOnly/<type>')
api.add_resource(ListClose, '/listCloseOnly')
api.add_resource(ListCloseType, '/listCloseOnly/<type>')
api.add_resource(AddTimes, '/addTimes')
api.add_resource(Register, '/api/register')
api.add_resource(Token, '/api/token')



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
