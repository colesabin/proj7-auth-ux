"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
from math import floor
import arrow
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

# Some variables for later.
# max and min speeds are from the table on https://rusa.org/pages/acp-brevet-control-times-calculator
# max_times is from https://rusa.org/pages/orgreg

max_speeds = [(1000, 26), (600,28), (400, 30), (200, 32), (0, 34)]
min_speeds = [(1000, 13.333), (600, 11.428), (0, 15)]
max_times = {200:13.5, 300:20, 400:27, 600:40, 1000:75}
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # Turn that nast iso string into a friendly arrow object
    arrow_time = arrow.get(brevet_start_time)
    # time starts at 0
    time = 0
    # If a controle point is past the final distance, use the final distance instead
    if (brevet_dist_km < control_dist_km):
        control_dist_km = brevet_dist_km
    # Iterate through max_speeds and add to time as needed.    
    for speed in max_speeds:
        dist, rate = speed
        while control_dist_km > dist:
            time += (control_dist_km - dist) / rate
            control_dist_km -= (control_dist_km - dist)
    # Get the hours and minutes as explained on https://rusa.org/pages/acp-brevet-control-times-calculator        
    hours = floor(time)
    minutes = round((time - hours) * 60)
    # Turn it back into an iso string and send it along.
    arrow_time = arrow_time.shift(hours=+hours, minutes=+minutes)
    return arrow_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #Almost totally the same as open_time() but with a couple of considerations
    arrow_time = arrow.get(brevet_start_time)
    time = 0
    # Controle points past the theoretical distance use a specific time
    if (brevet_dist_km <= control_dist_km):
        time = max_times[brevet_dist_km]
    # Controle points at 60km or less are special, 1 hr + dist/20
    elif (control_dist_km <= 60):
        time = (control_dist_km / 20) + 1
    # If neither of the above cases, then it works just like open_time()
    else :
        for speed in min_speeds:
            dist, rate = speed
            while control_dist_km > dist:
                time += (control_dist_km - dist) / rate
                control_dist_km -= (control_dist_km - dist)
    hours = floor(time)
    minutes = round((time - hours) * 60)
    arrow_time = arrow_time.shift(hours=+hours, minutes=+minutes)
    return arrow_time.isoformat()
